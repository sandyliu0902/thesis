<?php
require_once 'config.php';
require_once 'menu.php';

echo '<table width="1600" border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;font-size:13px;" align="center">';
foreach($GLOBAL_PERIODS as $key => $period)
{
    $time = array();
    array_push($time, microtime());

    $positive_power = bonacich_power($period['start'], $period['end'],  5, 0.5, 1, 1);
    $negative_power = bonacich_power($period['start'], $period['end'],  5, -0.5, 1, 1);
    $betweenness = betweenness($period['start'],$period['end'], 1);
    $degree = degree($period['start'], $period['end'], 1);
    $indegree = $degree['in'];
    $outdegree = $degree['out'];
    $alldegree = $degree['all'];
    $closeness = closeness($period['start'], $period['end'], 1);
    $inCloseness = $closeness['inCloseness'];
    $outCloseness = $closeness['outCloseness'];

    arsort($positive_power);
    arsort($negative_power);
    arsort($betweenness);
    arsort($indegree);
    arsort($outdegree);
    arsort($alldegree);
    arsort($inCloseness);
    arsort($outCloseness);

    $pz = normalize($positive_power);
    $nz = normalize($negative_power);
    $bz = normalize($betweenness);
    $iz = normalize($indegree);
    $oz = normalize($outdegree);
    $az = normalize($alldegree);
    $icz = normalize($inCloseness);
    $ocz = normalize($outCloseness);

    echo '<tr><td colspan="24" align="center" style="color:black;background:#FFFFFF;"><b>'.$period['start'].'~'.$period['end'].'</b></td></tr>';
    echo '<tr>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Bonacich\'s Power (beta+)</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Bonacich\'s Power (beta-)</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Betweenness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> inCloseness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> outCloseness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Indegree</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Outdegree</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Alldegree</b></td>
          </tr>';

    echo '<tr>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">power</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">power</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">betw</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">in/out</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">out/in</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">in/out</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">out/in</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">all</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          ';
    echo '</tr>';

    $tmp = array(0,0,0,0,0,0,0,0);
    for($j=0 ; $j<count($alldegree) ; $j++)
    {
        echo '<tr>';
        echo '<td style="color:'.(current($pz) > $GLOBAL_Z ? '000000' : '#999999').';">'.key($positive_power).'&nbsp;</td>
              <td style="color:'.(current($pz) > $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($positive_power) ,3,'.','').'</td>
              <td style="color:'.(current($pz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($pz) ,3,'.','').'</td>
              
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? '000000' : '#999999').';">'.key($negative_power).'&nbsp;</td>
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($negative_power) ,3,'.','').'</td>
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($nz) ,3,'.','').'</td>
              
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($betweenness).'&nbsp;</td>
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($betweenness) ,3,'.','').'</td>
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($bz) ,3,'.','').'</td>

              <td style="color:'.(current($icz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($inCloseness).'&nbsp;</td>
              <td style="color:'.(current($icz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($inCloseness) ,3,'.','').' / '.number_format($outCloseness[key($inCloseness)] ,3,'.','').'</td>
              <td style="color:'.(current($icz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($icz) ,3,'.','').'</td>
              
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($outCloseness).'&nbsp;</td>
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($outCloseness) ,3,'.','').' / '.number_format($inCloseness[key($outCloseness)] ,3,'.','').'</td>
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($ocz) ,3,'.','').'</td>
              
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($indegree).'&nbsp;</td>
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($indegree) ,0,'.','').' / '.number_format($outdegree[key($indegree)] ,0,'.','').'</td>
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($iz) ,3,'.','').'</td>
              
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($outdegree).'&nbsp;</td>
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($outdegree) ,0,'.','').' / '.number_format($indegree[key($outdegree)] ,0,'.','').'</td>
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($oz) ,3,'.','').'</td>
              
              <td style="color:'.(current($az) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($alldegree).'&nbsp;</td>
              <td style="color:'.(current($az) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($alldegree) ,0,'.','').'</td>
              <td style="color:'.(current($az) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($az) ,3,'.','').'</td>
              ';

        next($positive_power);
        next($negative_power);
        next($betweenness);
        next($indegree);
        next($outdegree);
        next($alldegree);
        next($inCloseness);
        next($outCloseness);
        
        next($pz);
        next($nz);
        next($bz);
        next($iz);
        next($oz);
        next($az);
        next($icz);
        next($ocz);
                
        echo '</tr>';
        
        if(current($pz) < $GLOBAL_Z) $tmp[0] = 1;
        if(current($nz) < $GLOBAL_Z) $tmp[1] = 1;
        if(current($bz) < $GLOBAL_Z) $tmp[2] = 1;
        if(current($iz) < $GLOBAL_Z) $tmp[3] = 1;
        if(current($oz) < $GLOBAL_Z) $tmp[4] = 1;
        if(current($az) < $GLOBAL_Z) $tmp[5] = 1;        
        if(current($icz) < $GLOBAL_Z) $tmp[6] = 1;
        if(current($ocz) < $GLOBAL_Z) $tmp[7] = 1;
        if(array_sum($tmp) == count($tmp)) break;
        
    }
}

echo '</table>';

mysql_close($link);

?>