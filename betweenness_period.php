<?php
require_once 'config.php';
require_once 'menu.php';

$query = "SELECT min(year) as minYear, max(year) as maxYear FROM codes WHERE year != '0000'";
$result = mysql_query($query);
$row = mysql_fetch_array($result, MYSQL_ASSOC);
$minYear = $row['minYear'];
$maxYear = $row['maxYear'];
mysql_free_result($result);




foreach($GLOBAL_PERIODS as $key => $period)
{
    $currentQuery = $query.' WHERE tCode.year <= '.$period['end'].' AND tCode.year >= '.$period['start'].' AND tCode.year != 0000';    
    $betweenness[$key] = betweenness($period['start'],$period['end']);
    arsort($betweenness[$key]);
    $z[$key] = normalize($betweenness[$key]);
}

echo '<table width="'.(count($betweenness)*350).'" border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;" align="center">';
echo '<tr>';
foreach($GLOBAL_PERIODS as $period)
{
    echo '<td colspan="3" align="center" style="color:white;background:#333333;"><b>'.$period['start'].'~'.$period['end'].'</b></td>';
}
echo '</tr>';
echo '<tr>';
foreach($GLOBAL_PERIODS as $period)
{
    echo '<td width="14%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="4%" style="background:#EEEEEE;color:#333333;font-weight:bold;">betw</td>
          <td width="4%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>';
}
echo '</tr>';


for($j=0 ; $j<count($betweenness[count($betweenness)-1]) ; $j++)
{
    echo '<tr>';
    for($i=0 ; $i<count($betweenness) ; $i++)
    {
        echo '<td style="color:'.(current($z[$i]) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($betweenness[$i]).'&nbsp;</td>
              <td style="color:'.(current($z[$i]) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($betweenness[$i]) ,3,'.','').'</td>
              <td style="color:'.(current($z[$i]) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($z[$i]) ,3,'.','').'</td>';
        next($betweenness[$i]);              
        next($z[$i]);
    }
    echo '</tr>';
}
echo '</table>';




mysql_close($link);

?>
