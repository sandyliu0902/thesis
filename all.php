<?php
require_once 'config.php';
require_once 'menu.php';

$query = "SELECT min(year) as minYear, max(year) as maxYear FROM codes WHERE year != '0000'";
$result = mysql_query($query);
$row = mysql_fetch_array($result, MYSQL_ASSOC);
$minYear = $row['minYear'];
$maxYear = $row['maxYear'];
mysql_free_result($result);


$startYear = isset($_GET['startYear']) ? $_GET['startYear'] : $minYear; // 統計起始年
$endYear = isset($_GET['endYear']) ? $_GET['endYear'] : $maxYear; // 統計結束年
$interval = isset($_GET['interval']) ? $_GET['interval'] : 1; // 每幾年唯一個時期
$isAccumulated = isset($_GET['isAccumulated']) && $_GET['isAccumulated'] == 'N' ? false : true; // 是否累計


?>
<br />
<center>
<form method="GET" id="f">
    時間:
        <select name="startYear" style="width:70px" onChange="document.getElementById('f').submit();">
            <?php 
                for($year=$minYear;$year<=$maxYear;$year++)
                {
                    if($year == $startYear)
                        echo '<option selected="selected" value="'.$year.'">'.$year.'</option>';
                    else
                        echo '<option value="'.$year.'">'.$year.'</option>';
                }
            ?>
        </select>
    年~
        <select name="endYear" style="width:70px" onChange="document.getElementById('f').submit();">
            <?php 
                for($year=$minYear;$year<=$maxYear;$year++)
                {
                    if($year == $endYear)
                        echo '<option selected="selected" value="'.$year.'">'.$year.'</option>';
                    else
                        echo '<option value="'.$year.'">'.$year.'</option>';
                }
            ?>
        </select>
    年，        
    間隔:
        <select name="interval" style="width:50px" onChange="document.getElementById('f').submit();">
            <?php 
                for($i=1;$i<=($maxYear-$minYear+2)/2;$i++)
                {
                    if($i == $interval)
                        echo '<option selected="selected" value="'.$i.'">'.$i.'</option>';
                    else
                        echo '<option value="'.$i.'">'.$i.'</option>';
                }
            ?>
        </select>
    年
    ，是否累計:
        <select name="isAccumulated" style="width:50px" onChange="document.getElementById('f').submit();">
            <option value="Y" <?php echo $isAccumulated ? 'selected="selected"':''; ?>>是</option>
            <option value="N" <?php echo !$isAccumulated ? 'selected="selected"':''; ?>>否</option>
        </select>
</form>
</center>
<?php

echo '<table width="1600" border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;font-size:13px;" align="center">';
for($year=$startYear ; $year<=$endYear ; $year+=$interval)
{
    if($isAccumulated)
    {
        $currentStartYear = $startYear;
    }
    else
    {
        $currentStartYear = $year;
    }
    
    $currentEndYear = ($year + $interval-1 > $endYear ? $endYear : $year + $interval-1 );


    $positive_power = bonacich_power($currentStartYear, $currentEndYear,  10, 0.5, 1);
    $negative_power = bonacich_power($currentStartYear, $currentEndYear,  10, -0.5, 1);
    $betweenness = betweenness($currentStartYear,$currentEndYear);
    $degree = degree($currentStartYear, $currentEndYear);
    $indegree = $degree['in'];
    $outdegree = $degree['out'];
    $alldegree = $degree['all'];
    $closeness = closeness($currentStartYear, $currentEndYear);
    $inCloseness = $closeness['inCloseness'];
    $outCloseness = $closeness['outCloseness'];


    arsort($positive_power);
    arsort($negative_power);
    arsort($betweenness);
    arsort($indegree);
    arsort($outdegree);
    arsort($alldegree);
    arsort($inCloseness);
    arsort($outCloseness);

    $pz = normalize($positive_power);
    $nz = normalize($negative_power);
    $bz = normalize($betweenness);
    $iz = normalize($indegree);
    $oz = normalize($outdegree);
    $az = normalize($alldegree);
    $icz = normalize($inCloseness);
    $ocz = normalize($outCloseness);

    echo '<tr><td colspan="24" align="center" style="color:black;background:#FFFFFF;"><b>'.$currentStartYear.'~'.$currentEndYear.'</b></td></tr>';
    echo '<tr>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Bonacich\'s Power (beta+)</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Bonacich\'s Power (beta-)</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Betweenness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> inCloseness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> outCloseness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Indegree</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Outdegree</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Alldegree</b></td>
          </tr>';

    echo '<tr>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">power</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">power</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">betw</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">in/out</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">out/in</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">in/out</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">out/in</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="8%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">all</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          ';
    echo '</tr>';

    $tmp = array(0,0,0,0,0,0,0,0);
    for($j=0 ; $j<count($alldegree) ; $j++)
    {
        echo '<tr>';
        echo '<td style="color:'.(current($pz) > $GLOBAL_Z ? '000000' : '#999999').';">'.key($positive_power).'&nbsp;</td>
              <td style="color:'.(current($pz) > $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($positive_power) ,3,'.','').'</td>
              <td style="color:'.(current($pz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($pz) ,3,'.','').'</td>
              
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? '000000' : '#999999').';">'.key($negative_power).'&nbsp;</td>
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($negative_power) ,3,'.','').'</td>
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($nz) ,3,'.','').'</td>
              
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($betweenness).'&nbsp;</td>
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($betweenness) ,3,'.','').'</td>
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($bz) ,3,'.','').'</td>

              <td style="color:'.(current($icz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($inCloseness).'&nbsp;</td>
              <td style="color:'.(current($icz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($inCloseness) ,3,'.','').' / '.number_format($outCloseness[key($inCloseness)] ,3,'.','').'</td>
              <td style="color:'.(current($icz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($icz) ,3,'.','').'</td>
              
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($outCloseness).'&nbsp;</td>
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($outCloseness) ,3,'.','').' / '.number_format($inCloseness[key($outCloseness)] ,3,'.','').'</td>
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($ocz) ,3,'.','').'</td>
              
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($indegree).'&nbsp;</td>
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($indegree) ,0,'.','').' / '.number_format($outdegree[key($indegree)] ,0,'.','').'</td>
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($iz) ,3,'.','').'</td>
              
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($outdegree).'&nbsp;</td>
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($outdegree) ,0,'.','').' / '.number_format($indegree[key($outdegree)] ,0,'.','').'</td>
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($oz) ,3,'.','').'</td>
              
              <td style="color:'.(current($az) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($alldegree).'&nbsp;</td>
              <td style="color:'.(current($az) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($alldegree) ,0,'.','').'</td>
              <td style="color:'.(current($az) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($az) ,3,'.','').'</td>
              ';

        next($positive_power);
        next($negative_power);
        next($betweenness);
        next($indegree);
        next($outdegree);
        next($alldegree);
        next($inCloseness);
        next($outCloseness);
        
        next($pz);
        next($nz);
        next($bz);
        next($iz);
        next($oz);
        next($az);
        next($icz);
        next($ocz);
                
        echo '</tr>';
        
        if(current($pz) < $GLOBAL_Z) $tmp[0] = 1;
        if(current($nz) < $GLOBAL_Z) $tmp[1] = 1;
        if(current($bz) < $GLOBAL_Z) $tmp[2] = 1;
        if(current($iz) < $GLOBAL_Z) $tmp[3] = 1;
        if(current($oz) < $GLOBAL_Z) $tmp[4] = 1;
        if(current($az) < $GLOBAL_Z) $tmp[5] = 1;        
        if(current($icz) < $GLOBAL_Z) $tmp[6] = 1;
        if(current($ocz) < $GLOBAL_Z) $tmp[7] = 1;
        if(array_sum($tmp) == count($tmp)) break;
        
    }
}

echo '</table>';

mysql_close($link);

?>