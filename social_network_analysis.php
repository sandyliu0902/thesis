<?php
require_once 'config.php';
require_once 'menu.php';

$query = "SELECT min(year) as minYear, max(year) as maxYear FROM codes WHERE year != '0000'";
$result = mysql_query($query);
$row = mysql_fetch_array($result, MYSQL_ASSOC);
$minYear = $row['minYear'];
$maxYear = $row['maxYear'];
mysql_free_result($result);


$divide = isset($_GET['divide']) ? $_GET['divide'] : 'period'; // 區分
$level = isset($_GET['level']) ? $_GET['level'] : 2; // 層級
$unit = isset($_GET['unit']) ? $_GET['unit'] : 3; // 單位
$isAccumulated = isset($_GET['isAccumulated']) && $_GET['isAccumulated'] == 'N' ? false : true; // 是否累計


?>
<br />
<center>
<form method="GET" id="f">
       區分:
        <select name="divide"  onChange="document.getElementById('f').submit();">
            <option value="year" <?php echo ($divide == 'year') ? 'selected="selected"':''; ?>>每年</option>
            <option value="period" <?php echo ($divide == 'period') ? 'selected="selected"':''; ?>>每時期</option>
        </select>
       層級:
        <select name="level"  onChange="document.getElementById('f').submit();">
            <option value="1" <?php echo ($level == 1) ? 'selected="selected"':''; ?>>十大構面</option>
            <option value="2" <?php echo ($level == 2) ? 'selected="selected"':''; ?>>操作元件</option>
        </select>
       單位:
        <select name="unit" onChange="document.getElementById('f').submit();">
            <option value="1" <?php echo ($unit == 1) ? 'selected="selected"':''; ?>>以事件為單位</option>
            <option value="2" <?php echo ($unit == 2) ? 'selected="selected"':''; ?>>以年為單位</option>
            <option value="3" <?php echo ($unit == 3) ? 'selected="selected"':''; ?>>以因果為單位</option>
        </select>
       統計函數:
        <select name="isAccumulated" onChange="document.getElementById('f').submit();">
            <option value="Y" <?php echo $isAccumulated ? 'selected="selected"':''; ?>>CDF</option>
            <option value="N" <?php echo !$isAccumulated ? 'selected="selected"':''; ?>>PDF</option>
        </select>
</form>
</center>
<?php
echo '<table width="1240" border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;font-size:13px;" align="center">';

if($divide == 'period')
{
    $periods = $GLOBAL_PERIODS;
}
elseif ($divide == 'year')
{
    $periods = array();
    for($year=$minYear ; $year<=$maxYear ; $year+=1)
    {
        if($isAccumulated)
        {
            $data['start'] = $minYear;
        }
        else
        {
            $data['start'] = $year;
        }
    
        $data['end'] = $year;
        array_push($periods, $data);
    }    
}

foreach($periods as $key => $period)
{
    if($level == 1)
    {
        $iterative = 5;
    }
    elseif($level == 2)
    {
        $iterative = 10;
    }
    
    $beta = 0.5;
    $alpha = 1;
    
    $positive_power = bonacich_power($period['start'], $period['end'],  $iterative, $beta, $alpha, $unit, $level);
    $negative_power = bonacich_power($period['start'], $period['end'],  $iterative, 0 - $beta, $alpha, $unit, $level);
    $betweenness = betweenness($period['start'],$period['end'], $unit, $level);
    $degree = degree($period['start'], $period['end'], $unit, $level);
    $indegree = $degree['in'];
    $outdegree = $degree['out'];
    $alldegree = $degree['all'];
    $closeness = closeness($period['start'], $period['end'], $unit, $level);
    $inCloseness = $closeness['inCloseness'];
    $outCloseness = $closeness['outCloseness'];


    arsort($positive_power);
    arsort($negative_power);
    arsort($betweenness);
    arsort($indegree);
    arsort($outdegree);
    arsort($alldegree);
    arsort($inCloseness);
    arsort($outCloseness);

    $pz = normalize($positive_power);
    $nz = normalize($negative_power);
    $bz = normalize($betweenness);
    $iz = normalize($indegree);
    $oz = normalize($outdegree);
    $az = normalize($alldegree);
    $icz = normalize($inCloseness);
    $ocz = normalize($outCloseness);

    echo '<tr><td colspan="24" align="center" style="color:black;background:#FFFFFF;"><b>'.$period['start'].'~'.$period['end'].'</b></td></tr>';
    echo '<tr>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Bonacich\'s Power (beta+)</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Bonacich\'s Power (beta-)</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Betweenness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> inCloseness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> outCloseness</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Indegree</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Outdegree</b></td>
          <td colspan="3" align="center" style="color:white;background:#333333;"><b> Alldegree</b></td>
          </tr>';

    echo '<tr align="Center">
          <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">指標</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">指標</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">指標</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">指標</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">指標</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">指標</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">指標</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="10%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">指標</td>
          <td width="1%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          ';
    echo '</tr>';

    $tmp = array(0,0,0,0,0,0,0,0);
    for($j=0 ; $j<count($alldegree) && $j<10 ; $j++)
    {
        echo '<tr>';
        echo '<td style="color:'.(current($pz) > $GLOBAL_Z ? '000000' : '#999999').';">'.key($positive_power).'&nbsp;</td>
              <td style="color:'.(current($pz) > $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($positive_power) ,1,'.','').'</td>
              <td style="color:'.(current($pz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($pz) ,1,'.','').'</td>
              
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? '000000' : '#999999').';">'.key($negative_power).'&nbsp;</td>
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($negative_power) ,1,'.','').'</td>
              <td style="color:'.(current($nz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($nz) ,1,'.','').'</td>
              
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($betweenness).'&nbsp;</td>
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($betweenness) ,1,'.','').'</td>
              <td style="color:'.(current($bz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($bz) ,1,'.','').'</td>

              <td style="color:'.(current($icz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($inCloseness).'&nbsp;</td>
              <td style="color:'.(current($icz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($inCloseness) ,1,'.','').'</td>
              <td style="color:'.(current($icz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($icz) ,1,'.','').'</td>
              
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($outCloseness).'&nbsp;</td>
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($outCloseness) ,1,'.','').'</td>
              <td style="color:'.(current($ocz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($ocz) ,1,'.','').'</td>
              
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($indegree).'&nbsp;</td>
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($indegree) ,0,'.','').'</td>
              <td style="color:'.(current($iz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($iz) ,1,'.','').'</td>
              
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($outdegree).'&nbsp;</td>
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($outdegree) ,0,'.','').'</td>
              <td style="color:'.(current($oz) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($oz) ,1,'.','').'</td>
              
              <td style="color:'.(current($az) >= $GLOBAL_Z ? '000000' : '#999999').';">'. key($alldegree).'&nbsp;</td>
              <td style="color:'.(current($az) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($alldegree) ,0,'.','').'</td>
              <td style="color:'.(current($az) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($az) ,1,'.','').'</td>
              ';

        next($positive_power);
        next($negative_power);
        next($betweenness);
        next($indegree);
        next($outdegree);
        next($alldegree);
        next($inCloseness);
        next($outCloseness);
        
        next($pz);
        next($nz);
        next($bz);
        next($iz);
        next($oz);
        next($az);
        next($icz);
        next($ocz);
                
        echo '</tr>';
        
        if(current($pz) < $GLOBAL_Z) $tmp[0] = 1;
        if(current($nz) < $GLOBAL_Z) $tmp[1] = 1;
        if(current($bz) < $GLOBAL_Z) $tmp[2] = 1;
        if(current($iz) < $GLOBAL_Z) $tmp[3] = 1;
        if(current($oz) < $GLOBAL_Z) $tmp[4] = 1;
        if(current($az) < $GLOBAL_Z) $tmp[5] = 1;        
        if(current($icz) < $GLOBAL_Z) $tmp[6] = 1;
        if(current($ocz) < $GLOBAL_Z) $tmp[7] = 1;
//        if(array_sum($tmp) == count($tmp ) break;
        
    }
}

echo '</table>';

mysql_close($link);

?>