<?php
require_once 'config.php';
require_once 'menu.php';

/*
 *  Parameter Setting
 */ 
 
$order = isset($_GET['order']) ? $_GET['order'] : 'in'; // 排序



?>
<br />
<center>
<form method="GET" id="f">
    排序:
        <select name="order" style="width:70px" onChange="document.getElementById('f').submit();">
            <option value="in" <?php echo ($order == 'in') ? 'selected="selected"':''; ?>>indegree</option>
            <option value="out" <?php echo ($order == 'out') ? 'selected="selected"':''; ?>>outdegree</option>
            <option value="all" <?php echo ($order == 'all') ? 'selected="selected"':''; ?>>alldegree</option>
        </select>
</form>
</center>
<?php


foreach($GLOBAL_PERIODS as $key => $period)
{
    $degree[$key] = degree($period['start'], $period['end']);

    switch($order)
    {
        case 'in':
            arsort($degree[$key]['in']);
            break;
        case 'out':
            arsort($degree[$key]['out']);
            break;
        case 'all':
            arsort($degree[$key]['all']);
            break;
    }    

    $z[$key]['out'] = normalize($degree[$key]['out']);
    $z[$key]['in'] = normalize($degree[$key]['in']);
    $z[$key]['all'] = normalize($degree[$key]['all']);
}

echo '<table width="'.(count($degree)*550).'" border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;" align="center">';
echo '<tr>';
foreach($GLOBAL_PERIODS as $period)
{
    echo '<td colspan="7" align="center" style="color:white;background:#333333;"><b>'.$period['start'].' ~ '.$period['end'].'</b></td>';
}
echo '</tr>';
echo '<tr>';
foreach($GLOBAL_PERIODS as $period)
{
    echo '<td width="6%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">in</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">out</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">all</td>
          <td width="2%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>';
}
echo '</tr>';

for($j=0 ; $j<count($degree[count($degree)-1]['all']) ; $j++)
{
    echo '<tr>';
    for($i=0 ; $i<count($degree) ; $i++)
    {
        $axial = key($degree[$i][$order]);
        echo '<td style="color:'.($z[$i][$order][$axial] >= $GLOBAL_Z ? '000000' : '#999999').';">'.$axial.'</td>
              <td style="color:'.($z[$i]['in'][$axial] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($degree[$i]['in'][$axial] ,0,'.','').'</td>
              <td style="color:'.($z[$i]['in'][$axial] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($z[$i]['in'][$axial] ,3,'.','').'</td>
              <td style="color:'.($z[$i]['out'][$axial] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($degree[$i]['out'][$axial] ,0,'.','').'</td>
              <td style="color:'.($z[$i]['out'][$axial] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($z[$i]['out'][$axial] ,3,'.','').'</td>
              <td style="color:'.($z[$i]['all'][$axial] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($degree[$i]['all'][$axial] ,0,'.','').'</td>
              <td style="color:'.($z[$i]['all'][$axial] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($z[$i]['all'][$axial] ,3,'.','').'</td>';
        
        next($degree[$i][$order]);   
    }
    echo '</tr>';
}
echo '</table>';


mysql_close($link);

?>
