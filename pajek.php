<?php
require_once 'config.php';
require_once 'menu.php';

function pajek($mainStartYear, $mainEndYear, $unit=3, $level=2, $nodeType='all', $compareStartYear=false, $compareEndYear=false )
{
    global $component;
    global $pajek_dir;

    if($nodeType != 'all')
    {
        $coreElements = findCoreElements($mainStartYear, $mainEndYear, $unit, $level);
    }
    
    if($compareStartYear && $compareEndYear)
    {
        if($nodeType != 'all')
        {
            $compareElements = findCoreElements($compareStartYear, $compareEndYear, $unit, $level);
            $compareRelations = findRelations($compareStartYear, $compareEndYear, $unit, $level, $compareElements);
        }
        else
        {
            $compareRelations = findRelations($compareStartYear, $compareEndYear, $unit, $level);
        }
    }
    else
    {
        $compareRelations = array();
    }


    $query = causeQuery($mainStartYear, $mainEndYear, $unit, $level);
    $result = mysql_query($query);

    $num = 0;
    $code = array();
    $fId = 0;
    $tId = 0;

    $Vertices = array();
    $Arcs = array();
    $partition = array();
    $i=0;
    while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {

        switch($level)
        {
            case 1:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromComponent'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toComponent'];
                break;
        
            case 2:
                $fCode = '['.$component[$row['fromComponent']].']'.$row['fromAxialCoding'];
                $tCode = '['.$component[$row['toComponent']].']'.$row['toAxialCoding'];
                break;
        }
        
        if($nodeType == 'core' && !(in_array($fCode, $coreElements) && in_array($tCode, $coreElements)) )
        {
            continue;
        }
        elseif($nodeType == 'surrounding' && (in_array($fCode, $coreElements) || in_array($tCode, $coreElements)) )
        {
            continue;
        }

        if(count($compareRelations) > 0 && isset($compareRelations[$fCode][$tCode]))
        {
            continue;
        }


        if(array_key_exists($fCode,$code))
        {
            $fId = $code[$fCode];
        }
        else
        {
            $fId = ++$num;
            $code[$fCode] = $fId;
            $Vertices[$fId] = $fCode;
            array_push($partition, $component[$row['fromComponent']]);
        }

        
        if(array_key_exists($tCode,$code))
        {
            $tId = $code[$tCode];
        }
        else
        {
            $tId = ++$num;
            $code[$tCode] = $tId;
            $Vertices[$tId] = $tCode;
            array_push($partition, $component[$row['toComponent']]);
        }

        array_push($Arcs,$fId.' '.$tId);
        
        $i++;
    }
echo $i;
echo '<br />';

    $content = '*Vertices '.count($Vertices).'
';

    foreach($Vertices as $k => $v )
    {
        $content .= $k .' '. '"'.$v.'"
';
    }

    $content .= '*Arcs
';
    foreach($Arcs as $a)
    {
        $content .= $a. '
';
    }

    $file = $pajek_dir.$mainStartYear.'-'.$mainEndYear.'_cause.net';
    $handle = fopen($file, 'w');
    fwrite($handle, mb_convert_encoding($content,'Big5','UTF-8'));
    fclose($handle);

    
    $content = '*Vertices '. count($partition).'
';
    foreach($partition as $p)
    {
        $content .= $p. '
';
    }

    $file = $pajek_dir.$mainStartYear.'-'.$mainEndYear.'_partition.clu';
    $handle = fopen($file, 'w');
    fwrite($handle, mb_convert_encoding($content,'Big5','UTF-8'));
    fclose($handle);
    
    
    mysql_free_result($result);
}

$nodeType = isset($_GET['nodeType']) ? $_GET['nodeType'] : 'all'; // 區分
$level = isset($_GET['level']) ? $_GET['level'] : 2; // 層級
$unit = isset($_GET['unit']) ? $_GET['unit'] : 3; // 單位
$isAccumulated = isset($_GET['isAccumulated']) && $_GET['isAccumulated'] == 'N' ? false : true; // 是否累計

?>
<br />
<center>
<form method="GET" id="f">
       層級:
        <select name="level">
            <option value="1" <?php echo ($level == 1) ? 'selected="selected"':''; ?>>十大構面</option>
            <option value="2" <?php echo ($level == 2) ? 'selected="selected"':''; ?>>操作元件</option>
        </select>
       單位:
        <select name="unit">
            <option value="1" <?php echo ($unit == 1) ? 'selected="selected"':''; ?>>以事件為單位</option>
            <option value="2" <?php echo ($unit == 2) ? 'selected="selected"':''; ?>>以年為單位</option>
            <option value="3" <?php echo ($unit == 3) ? 'selected="selected"':''; ?>>以因果為單位</option>
        </select>
       節點:
        <select name="nodeType">
            <option value="all" <?php echo ($nodeType == 'all') ? 'selected="selected"':''; ?>>所有元件(構面)</option>
            <option value="core" <?php echo ($nodeType == 'core') ? 'selected="selected"':''; ?>>核心元件(構面)</option>
            <option value="surrounding" <?php echo ($nodeType == 'surrounding') ? 'selected="selected"':''; ?>>非核心元件(構面)</option>
        </select>
        <input type="submit"/>
</form>
</center>
<?php


$isCompared = false;

if(isset($_GET['nodeType']))
{
    foreach($GLOBAL_PERIODS as $key => $period)
    {
        if($key == 0 || !$isCompared)
        {
            pajek($period['start'] ,$period['end'], $unit, $level, $nodeType);
        }
        else
        {        
            pajek($period['start'] ,$period['end'], $unit, $level, $nodeType, $GLOBAL_PERIODS[$key-1]['start'], $GLOBAL_PERIODS[$key-1]['end']);
        }
    }
    echo '<center><span style="font-family:arial">Plase find data at <u>'.mb_convert_encoding($pajek_dir,'utf8','big5').' </u></span></center>';
}
mysql_close($link);



?>
