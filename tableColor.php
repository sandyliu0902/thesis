<?php
require_once 'config.php';
require_once 'menu.php';

$result = mysql_query('
SELECT * FROM codes AS c1
WHERE component != "??" 
  AND year != "????"
  AND axialCodingForComponent1!= "??" 
  AND NOT EXISTS (SELECT * FROM codes AS c2 
               WHERE c1.component = c2.component
                 AND c1.axialCodingForComponent1 = c2.axialCodingForComponent1
                 AND c1.axialCodingForComponent2 = c2.axialCodingForComponent2
                 AND c1.axialCodingForComponent3 = c2.axialCodingForComponent3
                 AND c1.axialCodingForComponent4 = c2.axialCodingForComponent4
                 AND c1.year > c2.year
                 AND c1.id != c2.id)
GROUP BY component, axialCodingForComponent1, axialCodingForComponent2, axialCodingForComponent3, axialCodingForComponent4                  
ORDER BY component ASC, axialCodingForComponent1 ASC');

$tree = [];
while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {

//*
    if ($row['axialCodingForComponent1'] != '' && $row['axialCodingForComponent2'] == '') {
        if (!isset($tree[$row['component']]['&nbsp;']['&nbsp;']['&nbsp;'])) {
            $tree[$row['component']]['&nbsp;']['&nbsp;']['&nbsp;'] = array();
            $tree[$row['component']]['&nbsp;']['&nbsp;']['&nbsp;']['0000'] = '';
            for ($i = 1996; $i <= 2008; $i++) {
                $tree[$row['component']]['&nbsp;']['&nbsp;']['&nbsp;'][$i] = '';
            }
        }
        $tree[$row['component']]['&nbsp;']['&nbsp;']['&nbsp;'][$row['year']] += 1;
    } elseif ($row['axialCodingForComponent1'] != '' && $row['axialCodingForComponent2'] != '' && $row['axialCodingForComponent3'] == '') {
        $axialCodingForComponent1 = $row['axialCodingForComponent1'];
        if (!isset($tree[$row['component']][$row['axialCodingForComponent1']]['&nbsp;']['&nbsp;'])) {
            $tree[$row['component']][$axialCodingForComponent1]['&nbsp;']['&nbsp;'] = array();
            $tree[$row['component']][$axialCodingForComponent1]['&nbsp;']['&nbsp;']['0000'] = '';
            for ($i = 1996; $i <= 2008; $i++) {
                $tree[$row['component']][$axialCodingForComponent1]['&nbsp;']['&nbsp;'][$i] = '';
            }
        }
        $tree[$row['component']][$axialCodingForComponent1]['&nbsp;']['&nbsp;'][$row['year']] += 1;
    } elseif ($row['axialCodingForComponent1'] != '' && $row['axialCodingForComponent2'] != '' && $row['axialCodingForComponent3'] != '' && $row['axialCodingForComponent4'] == '') {
        if (!isset($tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']]['&nbsp;'])) {
            $tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']]['&nbsp;'] = array();
            $tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']]['&nbsp;']['0000'] = '';
            for ($i = 1996; $i <= 2008; $i++) {
                $tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']]['&nbsp;'][$i] = '';
            }
        }
        $tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']]['&nbsp;'][$row['year']] += 1;
    } else {
        if (!isset($tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']][$row['axialCodingForComponent3']])) {
            $tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']][$row['axialCodingForComponent3']] = array();
            $tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']][$row['axialCodingForComponent3']]['0000'] = '';
            for ($i = 1996; $i <= 2008; $i++) {
                $tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']][$row['axialCodingForComponent3']][$i] = '';
            }
        }
        $tree[$row['component']][$row['axialCodingForComponent1']][$row['axialCodingForComponent2']][$row['axialCodingForComponent3']][$row['year']] += 1;
    }
    /*  */
}

//*

echo '<table width="840" border="0" cellspacing="1" cellpadding="3" style="line-height:20px;font-size:11px" align="center">';
echo '<tr style="background:#333333;color:white;text-align:center">';
echo '<td width="10%">元件</td>';
echo '<td width="50%" colspan="3">主軸性編碼</td>';
echo '<td width="5%">0000</td>';
echo '<td width="5%">1996</td>';
echo '<td width="5%">1997</td>';
echo '<td width="5%">1998</td>';
echo '<td width="5%">1999</td>';
echo '<td width="5%">2000</td>';
echo '<td width="5%">2001</td>';
echo '<td width="5%">2002</td>';
echo '<td width="5%">2003</td>';
echo '<td width="5%">2004</td>';
echo '<td width="5%">2005</td>';
echo '<td width="5%">2006</td>';
echo '<td width="5%">2007</td>';
echo '<td width="5%">2008</td>';
echo '</tr>';
echo '<tr>';
Tree2Table(1, 5, $tree, true);
echo '</table>';

/*  */
mysql_free_result($result);
mysql_close($link);

?>
</style>